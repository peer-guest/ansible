# prometheus-server

This role sets up a prometheus server ready to scrape data from other hosts in
its network.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The main variables for this role are:

* `prometheus_targets_directory`: Directory to import exported target
                                  definitions from.
