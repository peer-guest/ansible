# streaming-frontend

Install and configure nginx to act as a streaming frontend server.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `debian_version`:                  Codename of the version of Debian used.

* `streaming.frontend.data_root`:    nginx data_root.

* `streaming.frontdend.server_names`: The FQDN of your frontend streaming servers.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
