# opsis-ingest

Depends on the `opsis` role.

Feed video from an Opsis into a voctomix.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `voctomix.host`:   Hostname or IP of the voctomix machine.

* `voctomix.port`:   Incoming port the voctomix machine listens to.

* `alsa_device`:     ALSA device used for USB audio capture.

* `audio_delay`:     Delay in ms for the audio capture.

* `video_delay`:     Delay in ms for the video capture.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
