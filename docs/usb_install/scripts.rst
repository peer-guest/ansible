.. _scripts:

Scripts
=======

:ref:`Quick Start <usb\_quick\_start>` allows you test your hardware and get
familiar with voctomix and the provisioning process. Do this before you start
trying to set up cameras and make other customizations.

``mk_usb_installer.sh``
-----------------------

This is the main script to build the USB installer. It makes a USB installer
that gets preseed configuration and runs ansible. It currently supports Debian
and Ubuntu.

The script is not self contained and needs bits & pieces from the ansible
repository and the internet to work properly.

A more complete documentation of this script :ref:`can be found here
<mk_usb_installer>`.

Step by step details
^^^^^^^^^^^^^^^^^^^^

Here is what the ``mk_usb_installer.sh`` script does:

1. Downloads an installation image (Debian or Ubuntu)
2. Sets kernel parameters to preseed the installer
3. Formats and mounts the USB key
4. Installs the installation image on the USB key
5. Unmounts the USB key

When you boot the USB installer:

1. Boot into d-i (the Debian Installer)
2. Use DHCP to configure a network interface
3. Fetch the preseed files from the http_server
4. Partition and format
5. Install Debian
6. git clone the playbook and inventory repos
7. Create some scripts to run ansible
8. Install Grub and reboot
9. Run the script created in step 7
10. Reboot



.. _httpserver:

``http_server.sh``
------------------

This script runs an HTTP server so that the USB installer can pull additional
configuration files. The root directory being served is
``roles/tftp-server/files``.

``test_thumb.sh``
-----------------

This script tests the USB stick by booting it in a QEMU x86_64 system. You will
need to install these dependencies and add yourself to the disk group::

    $ sudo apt install qemu qemu-utils qemu-system ovmf
    $ sudo adduser $USER disk

It takes one argument - the path to the USB device, for example ``/dev/sdb``::

    $ ./test_thumb.sh /dev/sdb

``test_pxe.sh``
---------------

This script tests the PXE booting environment by booting it in a QEMU x86_64
system. You will need to install those dependencies to run it properly::

    $ sudo apt install qemu brctl

You can then run the script this way::

    $ ./test_pxe.sh
