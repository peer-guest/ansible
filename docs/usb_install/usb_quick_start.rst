.. _usb_quick_start:

Quick Start
===========

This process gets you from nothing to something with minimal work.

Summary:

1. Run a script: downloads the installer and config to a usb stick.
2. Boot the usb stick to install an OS and setup voctomix.

.. WARNING::
   Following this procedure will wipe 2 disks:

   1. The usb stick you specify.
         The script asks for sudo privs to write an image to the usb device:
         ``zcat ${cache}/hd-media/boot.img.gz|sudo dcfldd of=${dev}``
   2. A disk on the machine you boot from this USB stick. Note if you have
      multiple drives: The installer may see the drives in a different order to
      your linux installs.

Let's get started:

1. Plug in a usb stick.
2. Figure out what dev it is.
3. Make sure it isn't mounted.
4. Run the following::

      $ sudo apt install curl git pmount dcfldd syslinux-efi syslinux-common
      $ git clone https://salsa.debian.org/debconf-video-team/ansible.git
      $ cd ansible/usbinst
      $ ./mk_usb_installer.sh /dev/sdX configs/voctotest.cfg

It will take a few minutes depending on your internet connection and usb speeds.
You will see:  "Please boot the target machine from the USB stick now."

Do it.  Use the stick to boot a second box.

The installer will prompt you for a few values:

1. hostname: voctotest
2. domain name: leave blank if you don't know.
3. password: whatever you want
4. disk partitioning method: Guided - use entire disk
5. disk drive: (the drive you want to wipe)
6. grub device: (likely the same drive)
7. and maybe some other parameters based on your hardware.

Sit back and watch as the installer installs the OS, reboots, ansible runs,  and reboots again.  At this point the machine should boot into X, auto-login, and start the backend parts of voctomix (voctocore, sources and sinks).

The last thing you need to do:
Click the Voctomix Lucky Cat icon in the tool bar.

That starts Voctogui, you should see 4 test patterns.

That's it.

Now you are ready to build a configuration based on the equipment and processes
that fit your requirements.
